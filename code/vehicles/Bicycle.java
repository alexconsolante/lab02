// Alexander Consolante
// ID:1931681
package vehicles;

public class Bicycle {
    
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public Bicycle(String varManufacturer,int varNumberGears,double varMaxSpeed) {

        this.manufacturer = varManufacturer;
        this.numberGears = varNumberGears;
        this.maxSpeed = varMaxSpeed;
    }

    public String toString() {

        return "Manufacturer: "+this.manufacturer+", Number of Gears: "+this.numberGears+", Max Speed: "+this.maxSpeed;
    }

    public String getManufacturer() {

        return this.manufacturer;

    }

    public int getGears() {

        return this.numberGears;

    }

    public double getMaxSpeed(){

        return this.maxSpeed;
    }
}
