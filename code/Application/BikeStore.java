// Alexander Consolante
// ID: 1931671
package Application;
import vehicles.*;
public class BikeStore {
    
    public static void main(String[] args) {
        
        Bicycle[] bikes = new Bicycle[4];
        bikes[0] = new Bicycle("Ford",4,55.5);
        bikes[1] = new Bicycle("Ferrari",8,100.10);
        bikes[2] = new Bicycle("Mclaren",7,120.1);
        bikes[3] = new Bicycle("Lamborghini",10,200.5);

        for (int i=0; i < bikes.length;i++) {
            System.out.println(bikes[i]);
        }
    }


}
